---
bookCollapseSection: true
weight: 13
---

# Enrollment Application Install

{{< hint info >}}
<b> <a href ="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an Android device.
{{< /hint >}}

Enforce applications to be installed during Android device enrollment.

<i>This configuration will be applied only during Android device enrollment.</i>

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Auto Install</strong></td>
            <td>When auto install is checked, then the applications that are selected will be installed autmatically.
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Work profile global user 
            configurations</strong></center>
                <table style="width: 100%;">
                    <tbody>
                        <tr>
                            <td rowspan="5">
                                <center><strong>App Auto Update Policy</strong></center>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>When connected to wi-fi</strong></td>
                        </tr>
                        <tr>
                            <td><strong>Auto Update any time</strong></td>
                        </tr>
                        <tr>
                            <td><strong>Ask user to Update</strong></td>
                        </tr>
                        <tr>
                            <td><strong>Disable Auto Update</strong></td>
                        </tr>
                        <tr>
                            <td rowspan="5">
                                <center><strong>App Availability To A User</strong></center>
                            </td>
                            <tr>
                                <tr>
                                    <td><strong>All Approved Apps for Enterprise</strong></td>
                                </tr>
                                <tr>
                                    <td><strong>All Apps from Playstores</strong></td>
                                </tr>
                                <tr>
                                    <td><strong>Only White-listed Apps</strong></td>
                                </tr>
                    </tbody>
                </table>
            </td>
            </tr>
            <tr>
                <td colspan="2">
                    <center><strong>App Install Policy(Work profile only)</strong></center>
                </td>
            </tr>
            <tr>
                <td><strong>App Initial Install Mode</strong>
                    <br>The auto install mode for the first time</td>
                <td>
                    <ul style="list-style-type:disc;">
                        <li>
                            Auto install once only when enrolling
                        </li>
                        <li>Do not install automatically</li>
                        <li>Auto install even if uninstalled manually</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td><strong>Priority level when installing the app</strong>
                    <br>Priority level when installing the app among many other apps</td>
                <td>
                    Lowest - Highest
                </td>
            </tr>
            <tr>
                <td><strong>
                                    Device charging state when installing apps
                                    </strong>
                    <br>Device charging state when installing apps</td>
                <td>
                    <ul style="list-style-type:disc;">
                        <li>Device must be charging</li>
                        <li>Device does not need to be charging</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td><strong>Device processor state when installing</strong>
                    <brDevice processor state when installing</td>
                        <td>
                            <ul style="list-style-type:disc;">
                                <li>
                                    Device does not need to be idling
                                </li>
                                <li>Device must be idling</li>
                            </ul>
                        </td>
            </tr>
            <tr>
                <td><strong>Device network state when installing</strong>
                    <br>Device processor state when installing</td>
                <td>
                    <ul style="list-style-type:disc;">
                        <li>
                            Device can be in any network
                        </li>
                        <li>Device must be in an unmetered network</li>
                    </ul>
                </td>
            </tr>
    </tbody>
</table>

{{< hint info >}}
<b> <a href = "https://entgra-documentation.gitlab.io/v3.7
.0/docs/guide-to-work-with-the-product/device-management-guide/android-device/android-device
-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}